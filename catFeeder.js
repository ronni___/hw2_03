


/*function feedCat1(){
    document.getElementById("photo1").innerHTML="aaaaaaaaaaaaaaa";
}

function feed(){
    const allradiobtn = document.querySelectorAll('input[class="btn"]');
    //let i = 0;
    for(const radiobtn of allradiobtn){ 
        
        if( radiobtn.checked ){
            if(radiobtn.id=="cat1"){
                feedCat1();
                
            }
            else if( radiobtn.value == "elegantie"){
                feedCat2();
            }
            else if( radiobtn.value == "rostie"){
                feedCat3();
            }
            else if( radiobtn.value == "uglie"){
                feedCat4();
            }
            else if( radiobtn.value == "naughtie"){
                feedCat5();
            }

        }

    }
}*/
const allCats = [
    {name: 'cutie', breed: 'baby', photo: 'cat1.jpg', photoInversed: 'cat1_inverse.jpg' },
    {name: 'elegantie', breed: 'white', photo: 'cat2.jpg', photoInversed: 'cat2_inverse.jpg' },
    {name: 'rostie', breed: 'baby', photo: 'cat3.jpg', photoInversed: 'cat3_inverse.jpg' },
    {name: 'uglie', breed: 'weird', photo: 'cat4.jpg', photoInversed: 'cat4_inverse.jpg' },
    {name: 'naughtie', breed: 'weird', photo: 'cat5.jpg', photoInversed: 'cat5_inverse.jpg' },
];

let toFeed = [];

function feed() {
    const inputBreed = document.getElementById("specificBreed");
    let tagFound = false;
    if (inputBreed.value.length > 0) {
        allCats.forEach(cat => {
            if (cat.breed == inputBreed.value) {
                tagFound = true;
                if('baby' == inputBreed.value){
                    //document.getElementById("photo1").src="feed/feedCat1";
                    const feedImage = document.createElement("img");
                    feedImage.classList.add("styleFeed");  //style.css
                    feedImage.src = "feed.png";
                    cat.container.appendChild(feedImage);
                }
            }
        })
        if (tagFound==false){
            alert(`The breed "${inputBreed.value}" doesn't exist!`);
        }
    }

}

